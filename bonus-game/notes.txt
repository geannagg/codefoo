Rules and other stuff:
- Random number (1 or 2) on who has the crib first (1=human; 2=AI)
- Play:
    - Combo of 15: 2 pts
    - Striaghts of min 3 cards: 1 pt per card
    - Pairs: 2 pts
    - Trio: 6 pts
    - Quad: 16 pts
    - *Jack of same suit of starter card: 1 pt
    - Get to 31 but not over
*Cannot factor these rules in the beginning: only in the end
    during scoring
- Scoring hand:
    - Combo of 15: 2 pts
    - Striaghts of min 3 cards: 1 pt per card
    - Pairs: 2 pts
    - Trio: 6 pts
    - Quad: 16 pts
    - Flush: 
    - *Jack of same suit of starter card: 1 pt
    - Flush 4 card (excluding starter): 4 pts
    - *Flush 5 card: 5 pts
- Game ends when there are no more cards
- Switch crib to other player
    - Crib mean that the play has two scorign hands
    - The cannot see the cards for scoring (ones given by the 
        opponent)
- Game ends when first player reaches 120 pts
AI:
- Play:

- Scoring: 