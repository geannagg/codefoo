# from card_game import Cribbage
import scoring
from player import Player, HumanPlayer, BotPlayer
from card import Card, CARD_NUMBER, SUIT_NUMBER
from deck import Hand


def can_move(count, hand):
    """Checks if the current player can play at least one card

    Keyword arguments:
    count -- int: total value of cards in play
    hand -- Hand: current player's hand

    @returns bool: False if player has no cards in hand, if all cards
                    in their hand + current played cards total to over 31
    """
    if (hand.get_hand_size() == 0):
        return False
    for card in hand.get_hand():
        if (card.get_value() + count <= 31):
            return True
    return False


def place_card(player, played_cards):
    """Player places a card in the cards in play by removing card from
        their hand and adds it into the cards in play

    Keyword arguments:
    player -- Player: current player that needs to put a card down
    played_cards -- Hand: cards in play so far (ordered by recent placement)

    @requires: can_move must be true before entering this functions
    """
    print('Here are your cards:')
    player.print_hand()
    while (True):
        card_pos = int(input('Which cards? (Choose by [position]) '))
        if (card_pos < 0 or card_pos >= player.get_hand_size()):
            print("Invalid card position.")
        else:
            if (player.get_hand().get_card(card_pos).get_value() +
                    scoring.get_total_count(played_cards) > 31):
                print("It's over 31. Try again.")
            else:
                player.remove_card(card_pos, played_cards)
                return True


# Need to add this to a testing suite
# card1 = Card('ACE', 'SPADES')
# card2 = Card('TWO', 'SPADES')
# card3 = Card('QUEEN', 'DIAMONDS')
# card4 = Card('THREE', 'CLUBS')
# card5 = Card('FOUR', 'CLUBS')
# card6 = Card('KING', 'HEARTS')

# cards1 = []
# cards2 = []
# empty_cards = []

# card1 = Card(CARD_NUMBER(1), SUIT_NUMBER(1))
# card2 = Card(CARD_NUMBER(2), SUIT_NUMBER(1))
# card3 = Card(CARD_NUMBER(12), SUIT_NUMBER(2))

# card4 = Card(CARD_NUMBER(3), SUIT_NUMBER(3))
# card5 = Card(CARD_NUMBER(4), SUIT_NUMBER(3))
# card6 = Card(CARD_NUMBER(13), SUIT_NUMBER(2))

# cards1.append(card1)
# cards1.append(card2)
# cards1.append(card3)

# cards2.append(card4)
# cards2.append(card5)
# cards2.append(card6)

# hand1 = Hand(cards1)
# hand2 = Hand(cards2)
# empty_hand = Hand(empty_cards)

# in_play = []
# start_card = Card(CARD_NUMBER(11), SUIT_NUMBER(2))

# in_play.append(start_card)

# in_play_hand = Hand(in_play)

# player = Player('P1', hand1)

# print('In play before: ')
# in_play_hand.print_hand()
# if (place_card(player, in_play_hand)):
#     print('can place')
# print('In play after: ')
# in_play_hand.print_hand()
# print('Player hand')
# player.print_hand()


def player_turn(player, count, played_cards, player2_score):
    if (not can_move(count, player.get_hand())):
        print('Can no longer go. Ending play.')
        return False

    command = input('(Round) What to do? ').strip()
    print('Scores: \n' + player.get_name() + str(player.get_score()) +
          '\t' + str(player2_score))
    if (command == 'hand'):
        print('Looking at hand')
        player.print_hand()
    elif (command == 'place'):
        place_card(player, count, played_cards)
        points = scoring.calculate_score(played_cards)
        player.add_score(points)
    elif (command == 'count'):
        print('Looking at current place count. Count: ' +
              scoring.get_total_count(played_cards))
    elif (command == 'end'):
        print('Stopping round and game.')
        return False
    else:
        print('Invalid command. Try again.')


def run_play(players):
    """Runs the play section of the game

    Keyword Arguments:
    players -- list<players>: players in the game (ordered by playing order)
    """
    playing = True
    total_count = 0
    while (playing):
        curr_p = 0
        next_p = 1
        print(players[curr_p].get_name() + ' is going')
        player_going = player_turn(players[curr_p], total_count,
                                   players[next_p].get_score())
        if (not player_going()):
            temp = curr_p
            curr_p = next_p
            next_p = temp
            continue


def run_show():
    pass

def switch_dealer(players):
    dealer = players[0]
    players[0] = players[1]
    players[1] = dealer

def play_round(players):
    print('Beginning round')
    playing = run_play(players)
    if (not playing):
        return False


def end_game():
    print('Ending game')
    return False


def run_game():
    print('Beginning game')
    is_running = True
    while (is_running):
        command = input('Start game? (y/n)').strip()
        if (command == 'n'):
            is_running = end_game()
        elif (command == 'y'):
            # Create a new cribbage game
            # game = Cribbage()
            players = []
            human_player = HumanPlayer('human')
            bot_player = BotPlayer('bot')
            players.append(human_player)
            players.append(bot_player)
            is_running = play_round(players)


# run_game()
