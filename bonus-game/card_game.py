from error_class import InputError
from player import HumanPlayer, BotPlayer
from deck import Deck


class CardGameFactory():
    def __init__(self, num_players=1):
        self.num_players = num_players

    def create_player(self): pass
    def create_players(self): pass
    def suit_system(self): pass
    def card_system(self): pass
    def create_deck(self): pass
    def scoring(self): pass
    def deal(self): pass


class Cribbage(CardGameFactory):
    def __init__(self):
        self.num_players = 2
        self.num_human = 1
        self.num_ai = 1
        if (self.num_human + self.num_ai > self.num_players):
            raise InputError(self.num_players, 'Cannot exceed number of \
                             players')

    def create_player(self, player_type, name):
        if (player_type == 'human'):
            return HumanPlayer(name)
        elif (player_type == 'bot'):
            return BotPlayer(name)
        else:
            raise InputError(player_type, 'Invalid player type')

    def create_players(self, num_humans=1, num_bots=1):
        human_players = []
        bot_players = []
        for i in range(num_humans):
            # Ask for name input
            human_name = input('Name for human please: ')
            human_players.append(self.create_player('human', human_name))
        for i in range(num_bots):
            bot_name = 'Bot No.' + i
            bot_players.append('bot', bot_name)

    def create_deck(self, num_decks):
        return Deck(num_decks)

    