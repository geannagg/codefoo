import copy
from error_class import InputError


class Player:
    def __init__(self, name, hand):
        self.name = name
        self.score = 0
        self.hand = hand

    def get_name(self):
        return self.name

    def get_score(self):
        return self.score

    def add_score(self, points):
        self.score += points

    def get_hand(self):
        return copy.deepcopy(self.hand)

    def get_hand_size(self):
        return self.hand.get_hand_size()

    def print_hand(self):
        self.hand.print_hand()

    def remove_card(self, card_pos, dest):
        if (card_pos > self.get_hand_size() or card_pos < 0):
            raise InputError(card_pos, 'Out of range')
        self.hand.remove_card(card_pos, dest)


class HumanPlayer(Player):    
    def is_human(self):
        return True


class BotPlayer(Player):
    # I need to add AI shit to this
    def is_bot(self):
        return True

    