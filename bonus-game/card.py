from enum import Enum
from error_class import InputError


class CARD_NUMBER(Enum):
    ACE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13


class SUIT_NUMBER(Enum):
    SPADES = 1
    HEARTS = 2
    CLUBS = 3
    DIAMONDS = 4


class CARD_VALUES:
    _CARD_VALUES = {CARD_NUMBER.ACE: 1,
                    CARD_NUMBER.TWO: 2,
                    CARD_NUMBER.THREE: 3,
                    CARD_NUMBER.FOUR: 4,
                    CARD_NUMBER.FIVE: 5,
                    CARD_NUMBER.SIX: 6,
                    CARD_NUMBER.SEVEN: 7,
                    CARD_NUMBER.EIGHT: 8,
                    CARD_NUMBER.NINE: 9,
                    CARD_NUMBER.TEN: 10,
                    CARD_NUMBER.JACK: 10,
                    CARD_NUMBER.QUEEN: 10,
                    CARD_NUMBER.KING: 10}

    def get_value(self, name):
        """Returns the value of the card according to corresponding name
        
        Keyword arguments:
        name -- CARD_NUMBER (Enum): card number inquiring about
        
        @returns -- int: assigned value of card
        @throws -- InputError: Given if card name isn't listed"""
        if (name not in self._CARD_VALUES):
            raise InputError(name, 'Invalid card name')
        else:
            return self._CARD_VALUES[name]

    def set_value(self, name, value):
        """Modify the a card's value

        Keyword arguments:
        name -- CARD_NUMBER (Enum): card number to change value
        value -- int: new value

        @modifies CARD_VALUES
        @throws InputError: Given if card name isn't listed
        """
        if (name not in self._CARD_VALUES):
            raise InputError(name, 'Invalid card name')
        else:
            self._CARD_VALUES[name] = value


class Card:
    def __init__(self, card_number, suit_type):
        self.card_number = card_number
        self.suit_type = suit_type

    def get_number(self):
        """Returns the card's type (e.g. ACE, ONE, etc.)
            as defined in the CARD_NUMBER Enum class"""
        return self.card_number.name

    def get_suit(self):
        """Returns the card's suit type (e.g. HEARTS, CLUBS, etc.)
            as defined in the SUIT_NUMBER Enum class"""
        return self.suit_type.name

    def get_value(self):
        """Returns the value of a card based on its card type"""
        card_values = CARD_VALUES()
        return card_values.get_value(self.card_number)

    def get_number_rep(self):
        return self.card_number.value

    def get_card_str(self):
        """Returns string representation of the card"""
        card_str = self.card_number.name + ' ' + self.suit_type.name + ': ' + \
            str(self.get_value())
        return card_str

    def print_card(self):
        """Prints the card's number, suit type, and value"""
        print(self.get_card_str())
