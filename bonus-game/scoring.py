from error_class import InputError


def get_total_count(played_cards):
    """Helper function to count the total value of played cards so far.

    Keyword arguments:
    played_cards -- Hand: cards in play so far (ordered by recent placement)

    @returns -- int: total value of card in the play
    """
    count = 0
    if (played_cards.is_empty()):
        raise InputError(played_cards, "Play cards shouldn't be empty")
    else:
        for card in played_cards.get_hand():
            count += card.get_value()
    return count


def get_run_points(played_cards):
    """Helper function to check if the last played card is part of a 
        run (sequence) of at least 3 cards. A run is +1/-1 sequence of
        of cards, irrespective of suit and value of card (only the name/
        number of the card matters).

    Keyword arguments:
    played_cards -- Hand: cards in play so far (ordered by recent placement)

    @returns -- int: points scored from creating a run; otherwise gives no
                        point (returns 0)
    """
    if (played_cards.get_hand_size() < 3):
        return 0
    prev_card = played_cards.get_card(played_cards.get_hand_size()-1)
    reversed_cards = played_cards.get_hand()
    reversed_cards.pop(-1)  # remove last played card
    reversed_cards = reversed(reversed_cards)
    count = 1
    for card in reversed_cards:
        if (card.get_number_rep() != prev_card.get_number_rep()-1 and
                card.get_number_rep() != prev_card.get_number_rep()+1):
            return 0
        prev_card = card
        count += 1
    return count


def which_pair(played_cards):
    """Helper function that checks if the cards in play is a pair,
        triple, or quad

    Keyword arguments:
    played_cards -- Hand: cards in play so far (ordered by recent placement)

    @returns -- int: count of adjacent cards that have the same number
                        (only starting from the most recent)
    """
    count = 0
    last_card_number = played_cards.get_hand()[-1].get_number()
    reversed_cards = reversed(played_cards.get_hand())
    for card in reversed_cards:
        if (card.get_number() == last_card_number):
            count += 1
        if (card.get_number() != last_card_number):
            break
    return count


def calculate_play_score(played_cards):
    """Calcuates additional points score during play (right after a player
        places a card).

    Rules for additional points:
    - Reaching total count of 15: 2 pts
    - Reaching total count of 31: 1 pt on top of winning/ending play pt
    - Pair in a row: 2 pts
    - Triple in a row: 6 pts
    - Quad in a row: 12 pts
    - Runs (sequence) of 3+: (1+ pts for additional cards after 3)
        - 3 in a row: 3 pts
        - 4 in a row: 4 pts
        - 5 in a row: 5 pts

    Keyword arguments:
    played_cards -- Hand: cards that are in play [last card is the 
                        card just placed by player]
    @returns -- int: points scored from placing card
    """
    score = 0
    if (get_total_count() == 15):
        score += 2
    elif (get_total_count() == 31):
        score += 1
    if (which_pair(played_cards) == 2):
        score += 2
    elif (which_pair(played_cards) == 3):
        score += 6
    elif (which_pair(played_cards) == 4):
        score += 12
    score += get_run_points(played_cards)
    return score
