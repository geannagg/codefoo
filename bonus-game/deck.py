import copy
from random import randint
from error_class import InputError
from card import Card, CARD_NUMBER, SUIT_NUMBER


class Hand:
    def __init__(self, initial_hand=[]):
        self.all_cards = initial_hand

    def get_hand(self):
        """Returns a copy of the list of cards in the hand"""
        return copy.deepcopy(self.all_cards)

    def get_card(self, pos):
        """Gives Card at the position pos

        Keyword arguments:
        pos -- int: position of cardhan

        @returns Card: card at that position
        @throws InputError if pos is out of range or card not in hand
        """
        if (pos > len(self.all_cards) or not self.all_cards):
            raise InputError(pos, "Invalid card position")
        if self.all_cards:
            return self.all_cards[pos]

    def is_empty(self):
        if (not self.all_cards):
            return True
        return False

    def get_hand_size(self):
        """Returns the number of cards in the hand"""
        return len(self.all_cards)

    def add_card(self, card):
        """Adds a card to the hand

        Keyword arguments:
        card -- Card: new card to be added to hand

        @modifies -- self.all_cards
        """
        self.all_cards.append(card)

    def remove_card(self, card_pos, dest):
        """Sends a card from this hand to another hand/deck.

        Keyword arguments:
        card -- int: position of card to be removed
        dest -- Hand or Deck: where card is moving to

        @return -- True if card was removed or
                   False if card was not
        @modifies -- self.all_cards
        @effects -- dest Hand/Deck adds a new card
        """
        if (card_pos > self.get_hand_size() or card_pos < 0):
            raise InputError(card_pos, 'Out of range')
        else:
            dest.add_card(self.all_cards[card_pos])
            self.all_cards.pop(card_pos)
        return False

    def print_hand(self):
        """Prints all the cards in the hand and their corresponding
            hand position"""
        for i, card in enumerate(self.all_cards):
            print('[' + str(i) + '] ' + card.get_card_str())


class Deck(Hand):
    def __init__(self, num_sets=1, num_suits=4, cards_per_suit=13):
        if (num_sets < 1):
            raise InputError(num_sets, 'There must at least be 1 deck')
        else:
            self.num_sets = num_sets
            for deck in range(num_sets):
                for suit in range(1, num_suits+1):
                    for card in range(1, cards_per_suit+1):
                        if (card == cards_per_suit+1):
                            break
                        new_card = Card(CARD_NUMBER(card), SUIT_NUMBER(suit))
                        self.all_cards.append(new_card)

    def get_hand(self):
        """Prevent revealing what's in the deck"""
        raise AttributeError("'Deck' object has no attribute 'get_hand'")

    def cut_deck(self):
        """Cuts the deck at a random number and reveals the next card"""
        value = randint(1, len(self.all_cards)-2)
        cut_card = self.get_card(value+1)
        cut_card_str = cut_card.get_card()
        print(cut_card_str)
        return cut_card

    def print_hand(self):
        """Prevent revealing what's in the deck"""
        raise AttributeError("'Deck' object has no attribute 'print_hand'")


