# My IGN's Code Foo 9 Submission

This repository contains the answers to be submitted to IGN's Code Foo 9. Here are how the challenges are mapped in this repository:

* (For everyone) Q2: [question2](https://bitbucket.org/geannagg/codefoo/src/master/question2/)
* (For everyone) Q3: [question3](https://bitbucket.org/geannagg/codefoo/src/master/question3/)
* Front End: [web-frontend](https://bitbucket.org/geannagg/codefoo/src/master/web-frontend/) 
* OPTIONAL BONUS: [bonus-game](https://bitbucket.org/geannagg/codefoo/src/master/bonus-game/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
npm v.6.7.0
node v.11.11.0
yarn v.1.13.0
Python v.3.6.3
```
Follow the instructions below to get the above running.

### Installing

To get the environment working, you need to install the prerequisites stated above. 

Please refer to to the the following links to install above.

* [npm](https://www.npmjs.com/get-npm)
* [node](https://nodejs.org/en/download/)
* [yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable) - This project mainly used yarn to install dependencies, run web applications, and builds
* [Python](https://docs.python.org/3/using/index.html)
* 

In the command line enter the following:

```
cd web-frontend
yarn install
```

to install all dependencies for the web application.

## Running Parts

### "Running" Question 2

Navigate to [question2](https://bitbucket.org/geannagg/codefoo/src/master/question2/) to see the answer for question 2. 

### Running Question 3

```
cd question3

python main.py
```
If you want to add try different a different inventory set, follow the same format as "armorer_inventory.txt". Change fname in the main.py to whatever the new text file is.

### Running web-frontend

Please refer to the [README.md](https://bitbucket.org/geannagg/codefoo/src/master/web-frontend/README.md) for running and building the web application.

### Running bonus-game

```
cd bonus-game

python main.py
```

## Built With

* [React](https://reactjs.org/) - The web framework used for the front end

## Author

* **Geanna Garcia** 