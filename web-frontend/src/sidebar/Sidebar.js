import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlay, faCheckCircle, faFileAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Sidebar.css';

library.add(faPlay)
library.add(faCheckCircle)
library.add(faFileAlt)

class Sidebar extends Component {
  render() {
    return (
      <div className="Sidebar">
        <button onClick={() => this.props.action('latest')}>
          <div className="label">
            <FontAwesomeIcon icon="check-circle" className="icon"/>
            Latest
          </div>
        </button>
        <button onClick={() => this.props.action('videos')}>
          <div className="label">
            <FontAwesomeIcon icon="play" className="icon"/> 
            Videos
          </div>
          </button>
        <button onClick={() => this.props.action('articles')}>
          <div className="label">
            <FontAwesomeIcon icon="file-alt" className="icon"/> 
            Articles
          </div>
          </button>
      </div>
    );
  }
}

export default Sidebar;
