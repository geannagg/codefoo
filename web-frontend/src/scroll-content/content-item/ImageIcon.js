import React, { Component } from 'react';

// import react-icons;



class ImageIcon extends Component {

  render() {
    return (
      <img src={this.props.imgFile[0].url} alt="article"/>
    );
  }
}

export default ImageIcon;