import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faComment } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(faComment)


class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: [{count:""}]
    };
  }

  componentDidMount() {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "https://ign-apis.herokuapp.com/comments?ids=";
    fetch(proxyurl + url + this.props.contentId)
      .then(response => response.json())
      .then(
        (result) => {
          this.setState({
            content: result.content
          })
        }
      )
  }


  render() {
    console.log(this.state.content)
      return (
        <div>
          <FontAwesomeIcon icon="comment"/>
          {this.state.content[0].count}
        </div>
      );
  }
}

export default Comment;