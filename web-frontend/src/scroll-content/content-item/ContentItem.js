import React, { Component } from 'react';
import './ContentItem.css';
import ImageIcon from './ImageIcon.js';
import Comment from './Comment.js';

class ContentItem extends Component {
  renderTitle() {
    let title;

    if (this.props.data.contentType === 'article') {
      title =
        <div className="title">
         {this.props.data.metadata.headline}
        </div>;
    }
    else if (this.props.data.contentType === 'video') {
        title =
        <div className="title">
            {this.props.data.metadata.title}
        </div>
    }
    return title;
  }  

  renderArticleTitle() {
    let title;

    if (this.props.data.contentType === 'article') {
      title =
        <div className="title">
         {this.props.data.metadata.headline}
        </div>;
    }
    return title;
  }    

  renderVideoTitle() {
    let title;
    if (this.props.data.contentType === 'video') {
        title =
        <div className="title">
            {this.props.data.metadata.title}
        </div>
    }
    return title;
  }  

  renderTime() {
    let publishTime;
    var datePublished = Date.parse(this.props.data.metadata.publishDate);
    var today = new Date();
    var age = today - datePublished;
    var timeLevels = [1000, 60, 60, 24, 365];
    var timeLevelsNames = ['s', 'm', 'h', 'd', 'y'];
    
    var curTime = age;
    var maxTimeLevelName = '';
    for (var i = 0; i < timeLevels.length; i++){
      if (curTime/timeLevels[i] > 1){
        curTime = Math.ceil(curTime / timeLevels[i]);
        maxTimeLevelName = timeLevelsNames[i];
      }
    }

    console.log(curTime);
    console.log(maxTimeLevelName);
    var contentAgeString = curTime;
    publishTime = 
    <p>
      {contentAgeString} {maxTimeLevelName}
    </p>;
    return publishTime;
  }

  renderImage() {
    let item;
    // Need to add logic for video image as well
    if (this.props.data.thumbnails.length > 0) {
      if (this.props.sortType == 'latest') {
        item =
        <ImageIcon imgFile={this.props.data.thumbnails} />;
      }
      else if (this.props.sortType == 'videos' &&
                this.props.data.contentType == 'video') {
        item =
        <ImageIcon imgFile={this.props.data.thumbnails} />;
      }
      else if (this.props.sortType == 'articles' &&
                this.props.data.contentType == 'article') {
        item =
        <ImageIcon imgFile={this.props.data.thumbnails} />;
      }
    } 
    return item;
  }


  renderItem() {
    let item;
    if (this.props.sortType == 'latest') {
      item = 
        <div className="grid-container">
          <div>
              {this.renderImage()}
          </div>
          <div>
              <div className="small-container small-header">
                <div className="col">
                  {this.renderTime()}
                </div>
                <div className="col">
                  <Comment contentId = {this.props.data.contentId} />
                </div>
              </div>
              {this.renderTitle()}
          </div>
      </div>;
    }
    else if (this.props.sortType == 'videos') {
      item = 
        <div className="grid-container">
          <div>
              {this.renderImage()}
          </div>
          <div>
              <div className="small-container small-header">
                <div className="col">
                  {this.renderTime()}
                </div>
                <div className="col">
                  <Comment contentId = {this.props.data.contentId} />
                </div>
              </div>
              {this.renderVideoTitle()}
          </div>
      </div>;
    }
    else if (this.props.sortType == 'articles') {
      item = 
        <div className="grid-container">
          <div>
              {this.renderImage()}
          </div>
          <div>
              <div className="small-container small-header">
                <div className="col">
                  {this.renderTime()}
                </div>
                <div className="col">
                  <Comment contentId = {this.props.data.contentId} />
                </div>
              </div>
              {this.renderArticleTitle()}
          </div>
      </div>;
    }
    return item;
  }

  //I need to get the time: currentTime - publishedTime
  render() {
    return (
      <div>
        {/* {this.renderTime()} */}
        {this.renderItem()}
      </div>
    );
  }
}

export default ContentItem;
