import React, { Component } from 'react';
import ContentItem from './content-item/ContentItem';

class ContentTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      sortType: 'latest',
      data: []
    };
  }

  componentDidMount() {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "https://ign-apis.herokuapp.com/content";
    fetch(proxyurl + url)
      .then(response => response.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            data: result.data
          })
        }
        ,
        (error) => {
          this.setState({
            isLoaded: true,
            error
          })
        }
      )
  }

  renderItem() {
    let item = [];
    if (this.props.sortType == 'latest') {
      for (var i = 0; i < this.state.data.length; i++) {
          item.push(
          <div>
            < ContentItem data={this.state.data[i]} sortType={this.props.sortType} />
          </div>);
        }
      }
    else if (this.props.sortType == 'videos') {
      for (var i = 0; i < this.state.data.length; i++) {
        if (this.state.data[i].contentType == 'video') {
          item.push(
          <div>
            < ContentItem data={this.state.data[i]} sortType={this.props.sortType} />
          </div>);
        }
      }
    }
    else if (this.props.sortType == 'articles') {
      for (var i = 0; i < this.state.data.length; i++) {
        if (this.state.data[i].contentType == 'article') {
          item.push(
          <div>
            < ContentItem data={this.state.data[i]} sortType={this.props.sortType} />
          </div>);
        }
      }
    }
    return item;
  }

  render() {
    const { error, isLoaded, data } = this.state;
    const showTable = true;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div>
          {this.renderItem()}
        </div>
      );
    }
  }
}

export default ContentTable;