import React, { Component } from 'react';
import './App.css';
import Sidebar from './sidebar/Sidebar.js';
import ContentTable from './scroll-content/ContentTable.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.sidebarHandler = this.sidebarHandler.bind(this);
    this.state= {
      sortType: "latest"
    };
  }
  
  sidebarHandler(e) {
    switch(e) {
      case "latest":
        this.setState({
          sortType: "latest"
        })
        break;
      case "videos":
        this.setState({
          sortType: "videos"
        })
       break;
      case "articles":
        this.setState({
          sortType: "articles"
        })
        break;
      default:
    }
      
  }


  render() {
    return (
      <div className="App">
        <h1 className="App-header">Latest News</h1>
        <div className="container">
          <div>
            <Sidebar action={this.sidebarHandler}  />
          </div>
          <div class="column">
              <ContentTable sortType={this.state.sortType} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
