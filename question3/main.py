import convert_json
import solution


def main():
    fname = 'armorer_inventory.txt'
    clean_file = convert_json.clean_armor_file(fname)
    inventory = solution.make_inventory(clean_file)
    print('Naive Solution: ')
    solution.naive_best_set(inventory)
    print('DP Solution: ')
    solution.dp_best_set(inventory)

if __name__ == "__main__":
    main()