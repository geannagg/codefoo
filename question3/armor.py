"""
File: armor.py

Contains Armor and ArmorSet classes
"""
import copy


class Armor:
    def __init__(self, armor_type, armor_name, armor_cost, armor_value):
        self.armor_type = armor_type
        self.armor_name = armor_name
        self.armor_cost = int(armor_cost)
        self.armor_value = int(armor_value)

    def get_type(self):
        return self.armor_type

    def get_name(self):
        return self.armor_name

    def get_cost(self):
        return self.armor_cost

    def get_value(self):
        return self.armor_value

    def __eq__(self, other_armor):
        if (other_armor is None):
            return False
        if (self.armor_type == other_armor.get_type() and
                self.armor_name == other_armor.get_name() and
                self.armor_cost == other_armor.get_cost() and
                self.armor_value == other_armor.get_value()):
            return True
        return False

    def get_armor_str(self):
        """Returns the string version of the armor, including its
            type, name, cost, and value
        """
        armor = self.armor_type + ': ' + self.armor_name + '; Cost: ' + \
            str(self.armor_cost) + '; Value: ' + str(self.armor_value)
        return armor


class ArmorSet:
    """
    armor_set = {
        "helmet": Armor #must only be Helmet type
        "chest": Armor #must only be Chest type
        "leggings": Armor #must only be Leggings type
        "boots": Armor #must only be Boots type
        "extra": Armor #can be of any type
    }
    """
    def __init__(self):
        """Initialize ArmorSet with a helmet, chest, leggings, boots,
            and extra Armor; sets values to None if the fields are empty
        """
        self.armor_set = {}
        self.armor_set.setdefault('helmet')
        self.armor_set.setdefault('chest')
        self.armor_set.setdefault('leggings')
        self.armor_set.setdefault('boots')
        self.armor_set.setdefault('extra')

    def is_empty(self):
        if (not self.armor_set):
            return True
        return False

    def get_armor_set(self):
        return copy.deepcopy(self.armor_set)

    def get_total_cost(self):
        """Returns total cost of armor set"""
        if (self.is_empty()):
            return 0
        cost = 0
        for armor_type, armor in self.armor_set.items():
            if (armor is None):
                continue
            else:
                cost += armor.get_cost()
        return cost

    def get_total_value(self):
        """Returns total value of armor set"""
        if (self.is_empty()):
            return 0
        value = 0
        for armor_type, armor in self.armor_set.items():
            if (armor is None):
                continue
            else:
                value += armor.get_value()
        return value

    def get_armor(self, armor_type):
        """Return armor at the specified armor_type

        Keyword arguments:
        armor_type -- str: helmet, chest, leggings, boots, extra
        """
        return self.armor_set.get(armor_type)

    def __eq__(self, other_set):
        if (self.get_helmet() == other_set.get_helmet() and
                self.get_chest() == other_set.get_chest() and
                self.get_leggings() == other_set.get_leggings() and
                self.get_boots() == other_set.get_boots() and
                self.get_extra() == other_set.get_extra()):
            return True
        return False

    def remove_armor(self, armor_type):
        """Remove the armor in the set

        Keyword arguments:
        armor_type -- str: helmet/chest/leggings/boots/extra
        """
        self.armor_set[armor_type] = None

    def set_armor(self, armor_type, armor):
        """Updates armor for the specified armor type

        Keyword arguments:
        armor_type -- str: helmet/chest/leggings/boots/extra
        armor -- Armor: the armor to be set in ArmorSet
        """
        self.armor_set[armor_type] = armor

    def print_armor_set(self):
        """Print the pieces in the armor set, total cost, and total value.
        """
        total_val = str(self.get_total_value())
        total_cost = str(self.get_total_cost())
        print('---------------------------------------------------------')
        print('Total value: ' + total_val)
        print('Total cost: ' + total_cost)
        for armor_type, armor in self.armor_set.items():
            if (armor is None):
                print(armor)
            else:
                print(armor_type + ': ' + armor.get_armor_str())
        print('---------------------------------------------------------')
