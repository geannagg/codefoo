import copy
import json
from armor import Armor, ArmorSet


def make_inventory(inventory_json):
    """Adds each object in the JSON into a list<Armor>, where each
        object is type Armor

    Keyword arguments:
    inventory_json -- str: name of file of inventory
    """
    inventory = []
    data = json.load(open(inventory_json))
    for item in data:
        armor = Armor(item.get('armorType'), item.get('armorName'), 
                      item.get('armorCost'), item.get('armorValue'))
        inventory.append(armor)
    return inventory


def naive_best_set(inventory, threshold=300):
    """Finds the best (highest value) armor set below a cost of 300 crowns
        with at least one of each armor type

    Keyword arguments:
    inventory -- list<Armor>: all the armor provided in pdf
    threshold -- int: available crowns; default at 300
    """
    helmets = []
    chests = []
    leggings = []
    boots = []
    for item in inventory:
        if (item.get_type() == 'Helmet'):
            helmets.append(item)
        elif (item.get_type() == 'Chest'):
            chests.append(item)
        elif (item.get_type() == 'Leggings'):
            leggings.append(item)
        elif (item.get_type() == 'Boots'):
            boots.append(item)

    armor_set = ArmorSet()
    best_set = copy.deepcopy(armor_set)

    for helmet in helmets:
        for chest in chests:
            for legging in leggings:
                for boot in boots:
                    for item in inventory:
                        if (item == helmet or
                                item == chest or
                                item == legging or
                                item == boot):
                            continue
                        armor_set.set_armor('helmet', helmet)
                        armor_set.set_armor('chest', chest)
                        armor_set.set_armor('leggings', legging)
                        armor_set.set_armor('boots', boot)
                        armor_set.set_armor('extra', item)
                        if (armor_set.get_total_cost() <= threshold and
                                armor_set.get_total_value() >=
                                best_set.get_total_value()):
                            best_set = copy.deepcopy(armor_set)
    best_set.print_armor_set()
    return best_set


def dp_best_set(inventory, threshold=300):
    """Finds the best (highest value) armor set below a cost of 300 crowns
        with at least one of each armor type using Dynamic Programming

    Keyword arguments:
    inventory -- list<Armor>: all the armor provided in pdf
    threshold -- int: available crowns; default at 300
    """
    helmets = []
    chests = []
    leggings = []
    boots = []
    for item in inventory:
        if (item.get_type() == 'Helmet'):
            helmets.append(item)
        elif (item.get_type() == 'Chest'):
            chests.append(item)
        elif (item.get_type() == 'Leggings'):
            leggings.append(item)
        elif (item.get_type() == 'Boots'):
            boots.append(item)

    all_armor_pieces = []
    all_armor_pieces.append([])
    all_armor_pieces.append(helmets)
    all_armor_pieces.append(chests)
    all_armor_pieces.append(leggings)
    all_armor_pieces.append(boots)
    all_armor_pieces.append(inventory)
    all_armor_types = ['', 'helmet', 'chest', 'leggings', 'boots', 'extra']
    memo = []
    empty_armor_set = ArmorSet()
    for i in range(0, threshold+1):
        row = []
        for j in range(0, 6):
            row.append(copy.deepcopy(empty_armor_set))
        memo.append(row)

    for cost in range(0, threshold+1):
        for i, armor_type in enumerate(all_armor_pieces):
            for armor in armor_type:
                new_cost = cost - armor.get_cost()
                # Prevents wrap around in memo
                if (new_cost < 0):
                    continue
                armor_set = copy.deepcopy(memo[new_cost][i-1])
                # Prevents duplicates from entering ArmorSet
                if (armor in armor_set.get_armor_set().values()):
                    continue
                # Ensures there's at least one of each type
                armor_count = 0
                for item in armor_set.get_armor_set().values():
                    if (item is not None):
                        armor_count += 1
                if (armor_count < i-1):
                    continue
                armor_set.set_armor(all_armor_types[i], armor)
                # Checks to see if current ArmorSet is better than last
                if (armor_set.get_total_value() >=
                        memo[cost][i].get_total_value()):
                    memo[cost][i] = armor_set

    memo[len(memo)-1][len(memo[0])-1].print_armor_set()
    return memo[len(memo)-1][len(memo[0])-1]

