"""Cleans text files for readable json object."""
import json


def clean_armor_file(dirty):
    """Cleans a block of text (armor inventory) and returns a
        returns JSON list

    Keyword Arguments:
    dirty -- file name of text to clean
    """
    clean_file = 'clean_inventory.json'
    inventory = []
    with open(dirty) as infile:
        line = infile.readline()
        item = {}
        while line:
            new_line = line.strip(' \n.')
            split_line = new_line.split(' ')
            armor_type = split_line[0]
            armor_name = ' '.join(split_line[1:-2])
            armor_cost = split_line[-2]
            armor_value = split_line[-1]
            item['armorType'] = armor_type
            item['armorName'] = armor_name
            item['armorCost'] = armor_cost
            item['armorValue'] = armor_value
            inventory.append(item.copy())
            line = infile.readline()
    with open(clean_file, 'w') as outfile:
        json.dump(inventory, outfile, indent=4)
    return clean_file

